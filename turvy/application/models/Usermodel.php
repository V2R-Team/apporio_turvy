<?php
class Usermodel extends CI_Model{
	
	function payment($payment_option_id)
	{
		$data = $this->db->select('*')
                         ->from('payment_option')          
                         ->where(['payment_option_id'=>$payment_option_id])
                         ->get();
        return $data->row();
	}
	
    function user_rides($user_id)
    {
        $data = $this->db->select('*')
                         ->from('table_user_rides')
                         ->order_by("user_ride_id", "desc")
                         ->where(['user_id'=>$user_id])
                         ->get();
        return $data->result_array();
    }

    function normal_ride($booking_id)
    {
        $data = $this->db->select('*')
                         ->from('ride_table')
                         ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')
                         ->join('user','user.user_id=ride_table.user_id','inner')
                         ->where(['ride_table.ride_id'=>$booking_id])
                         ->get();
        return $data->row();
    }

    function active_normal_ride($booking_id)
    {
        $data = $this->db->select('*')
                         ->from('ride_table')
                         ->where(['ride_id'=>$booking_id])
                         ->where('payment_status',0)
                         ->where_in('ride_status', array('1','3','5','6','7','31'))
                         ->get();
        return $data->row();
    }

    function active_normal_ride2($booking_id)
    {
        $data = $this->db->select('*')
            ->from('ride_table')
            ->where(['ride_id'=>$booking_id])
            ->where('payment_status',0)
            ->where_in('ride_status', array('1','3','5','6','7','30','31'))
            ->get();
        return $data->row();
    }

    function completed_normal_ride($booking_id)
    {
        $data = $this->db->select('*')
                          ->from('ride_table')
                          ->where(['ride_id'=>$booking_id])
                          ->where('payment_status',1)
                           ->where_in('ride_status', array('7'))
                           ->get();
        return $data->row();
    }

    function driver_active_normal_ride($booking_id)
    {
        $data = $this->db->select('*')
            ->from('ride_table')
            ->where(['ride_id'=>$booking_id])
            ->where_in('ride_status', array('1','3','5','6','31'))
            ->get();
        return $data->row();
    }

    function driver_active_normal_ride2($booking_id)
    {
        $data = $this->db->select('*')
            ->from('ride_table')
            ->where(['ride_id'=>$booking_id])
            ->where_in('ride_status', array('1','3','5','6','30','31'))
            ->get();
        return $data->row();
    }



    function normal_done_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('done_ride')
                        ->join('driver','driver.driver_id=done_ride.driver_id','inner')
                        ->where(['done_ride.ride_id'=>$booking_id])
                        ->get();
        return $data->row();
    }

    function rental_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('rental_booking')
                        ->join('car_type','car_type.car_type_id=rental_booking.car_type_id','inner')
                        ->join('user','user.user_id=rental_booking.user_id','inner')
                        ->where(['rental_booking.rental_booking_id'=>$booking_id])
                        ->get();
        return $data->row();
    }


    function active_rental_ride($booking_id)
    {
        $data = $this->db->select('*')
                         ->from('rental_booking')
                         ->where('payment_status',0)
                         ->where(['rental_booking_id'=>$booking_id])
                         ->where_in('booking_status', array('10','11','12','13','16','33'))
                         ->get();
        return $data->row();
    }

    function active_rental_ride2($booking_id)
    {
        $data = $this->db->select('*')
            ->from('rental_booking')
            ->where('payment_status',0)
            ->where(['rental_booking_id'=>$booking_id])
            ->where_in('booking_status', array('10','11','12','13','16','32','33'))
            ->get();
        return $data->row();
    }

    function completed_rental_ride($booking_id)
    {
        $data = $this->db->select('*')
                         ->from('rental_booking')
                         ->where('payment_status',1)
                         ->where(['rental_booking_id'=>$booking_id])
                         ->where_in('booking_status', array('16'))
                         ->get();
        return $data->row();
    }

    function driver_active_rental_ride($booking_id)
    {
        $data = $this->db->select('*')
            ->from('rental_booking')
            ->where(['rental_booking_id'=>$booking_id])
            ->where_in('booking_status', array('10','11','12','13','33'))
            ->get();
        return $data->row();
    }

    function driver_active_rental_ride2($booking_id)
    {
        $data = $this->db->select('*')
            ->from('rental_booking')
            ->where(['rental_booking_id'=>$booking_id])
            ->where_in('booking_status', array('10','11','12','13','32','33'))
            ->get();
        return $data->row();
    }

    function rental_done_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('table_done_rental_booking')
                        ->join('driver','driver.driver_id=table_done_rental_booking.driver_id','inner')
                        ->where(['rental_booking_id'=>$booking_id])
                        ->get();
        return $data->row();
    }

    function coupan($coupon_code)
    {
        $data = $this->db->select('*')
                            ->from('coupons')
                            ->where(['coupons_code'=>$coupon_code])
                            ->get();
        return $data->row();
    }

}