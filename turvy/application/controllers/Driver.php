<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Driver extends REST_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('Drivermodel');
        $this->load->model('Usermodel');
    }

    public function Driver_Start_To_User_Location_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $booking_id = $this->post('booking_id');
        $ride_mode = $this->post('ride_mode');
        if(!empty($booking_id) && !empty($ride_mode) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $last_time_stamp = date("h:i:s A");
                if ($ride_mode == 1)
                {
                    $ride_details = $this->Usermodel->normal_ride($booking_id);
                    if (!empty($ride_details))
                    {
                        $ride_status = $ride_details->ride_status;
                        if ($ride_status == 30)
                        {
                            $query = array('driver_id'=>$driver_id,'ride_status'=>31,'last_time_stamp'=>$last_time_stamp);
                            $this->Drivermodel->ride_later_Accept($query,$booking_id);
                            $user_id = $ride_details->user_id;
                            $message = "Driver Start Towards Pickup Location";
                            $ride_id= (String) $booking_id;
                            $ride_status= "31";
                            $query = $this->Rentalmodel->user_device($user_id);
                            $pem_file = $ride_details->pem_file;
                            if (!empty($query))
                            {
                                foreach ($query as $item)
                                {
                                    $device_id = $item->device_id;
                                    $flag = $item->flag;
                                    if ($flag == 1)
                                    {

                                        $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                    }
                                    else
                                    {
                                        $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                    }
                                }
                            }else{
                                $device_id = $ride_details->device_id;
                                $flag = $ride_details->flag;
                                if ($flag == 1)
                                {
                                    $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                }
                                else
                                {
                                    $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                }
                            }
                            $ride_details = $this->Usermodel->normal_ride($booking_id);
                            $this->response([
                                'status' => 1,
                                'message' => 'Driver Start Towards Pickup Location',
                                'details'=>$ride_details
                            ], REST_Controller::HTTP_CREATED);
                        }else{
                            $this->response([
                                'status' => 0,
                                'message' => 'Sorry Ride Expire'
                            ], REST_Controller::HTTP_CREATED);
                        }
                    }else{
                        $this->response([
                            'status' => 0,
                            'message' => 'No Record Found'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $ride_details = $this->Usermodel->rental_ride($booking_id);
                    if (!empty($ride_details))
                    {
                        $booking_status = $ride_details->booking_status;
                        if ($booking_status == 32)
                        {
                            $query = array('driver_id'=>$driver_id,'booking_status'=>33,'last_update_time'=>$last_time_stamp);
                            $this->Drivermodel->ride_rental_later_Accept($query,$booking_id);
                            $user_id = $ride_details->user_id;
                            $message = "Driver start Towards Pick Up Location";
                            $ride_id= (String) $booking_id;
                            $ride_status= "33";
                            $query = $this->Rentalmodel->user_device($user_id);
                            $pem_file = $ride_details->pem_file;
                            if (!empty($query))
                            {
                                foreach ($query as $item)
                                {
                                    $device_id = $item->device_id;
                                    $flag = $item->flag;
                                    if ($flag == 1)
                                    {

                                        $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                    }
                                    else
                                    {
                                        $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                    }
                                }
                            }else{
                                $device_id = $ride_details->device_id;
                                $flag = $ride_details->flag;
                                if ($flag == 1)
                                {
                                    $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                }
                                else
                                {
                                    $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                }
                            }
                            $ride_details = $this->Usermodel->rental_ride($booking_id);
                            $this->response([
                                'status' => 1,
                                'message' => 'Ride Accpet Succesfully',
                                'details'=>$ride_details
                            ], REST_Controller::HTTP_CREATED);
                        }else{
                            $this->response([
                                'status' => 0,
                                'message' => 'Sorry Ride Expire'
                            ], REST_Controller::HTTP_CREATED);
                        }
                    }else{
                        $this->response([
                            'status' => 0,
                            'message' => 'No Record Found'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    public function Cancel_Ride_After_Accpet_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $booking_id = $this->post('booking_id');
        $ride_mode = $this->post('ride_mode');
        if(!empty($booking_id) && !empty($ride_mode) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $last_time_stamp = date("h:i:s A");
                if ($ride_mode == 1)
                {
                    $ride_details = $this->Usermodel->normal_ride($booking_id);
                    if (!empty($ride_details))
                    {
                        $query = array('driver_id'=>0,'ride_status'=>1,'last_time_stamp'=>$last_time_stamp);
                        $this->Drivermodel->ride_later_cancel($query,$booking_id);
                            $this->response([
                                'status' => 1,
                                'message' => 'Ride Cancel Succesfully'
                            ], REST_Controller::HTTP_CREATED);

                    }else{
                        $this->response([
                            'status' => 0,
                            'message' => 'No Record Found'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $ride_details = $this->Usermodel->rental_ride($booking_id);
                    if (!empty($ride_details))
                    {
                            $query = array('driver_id'=>0,'booking_status'=>10,'last_update_time'=>$last_time_stamp);
                            $this->Drivermodel->ride_rental_later_cancel($query,$booking_id);
                            $this->response([
                                'status' => 1,
                                'message' => 'Ride Cancel Succesfully',
                                'details'=>$ride_details
                            ], REST_Controller::HTTP_CREATED);
                    }else{
                        $this->response([
                            'status' => 0,
                            'message' => 'No Record Found'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    public function Ride_later_Accpet_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $booking_id = $this->post('booking_id');
        $ride_mode = $this->post('ride_mode');
        if(!empty($booking_id) && !empty($ride_mode) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $last_time_stamp = date("h:i:s A");
                if ($ride_mode == 1)
                {
                    $ride_details = $this->Usermodel->normal_ride($booking_id);
                    if (!empty($ride_details))
                    {
                       $ride_status = $ride_details->ride_status;
                       if ($ride_status == 1)
                       {
                           $query = array('driver_id'=>$driver_id,'ride_status'=>30,'last_time_stamp'=>$last_time_stamp);
                           $this->Drivermodel->ride_later_Accept($query,$booking_id);
                           $user_id = $ride_details->user_id;
                           $message = "Driver Accept Your Ride";
                           $ride_id= (String) $booking_id;
                           $ride_status= "30";
                           $query = $this->Rentalmodel->user_device($user_id);
                           $pem_file = $ride_details->pem_file;
                           if (!empty($query))
                           {
                               foreach ($query as $item)
                               {
                                   $device_id = $item->device_id;
                                   $flag = $item->flag;
                                   if ($flag == 1)
                                   {

                                       $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                   }
                                   else
                                   {
                                       $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                   }
                               }
                           }else{
                               $device_id = $ride_details->device_id;
                               $flag = $ride_details->flag;
                               if ($flag == 1)
                               {
                                   $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                               }
                               else
                               {
                                   $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                               }
                           }
                           $this->response([
                                       'status' => 1,
                                       'message' => 'Ride Accpet Succesfully',
                                       'details'=>$ride_details
                                   ], REST_Controller::HTTP_CREATED);
                       }else{
                           $this->response([
                                       'status' => 0,
                                       'message' => 'Sorry Ride Expire'
                                   ], REST_Controller::HTTP_CREATED);
                       }
                    }else{
                        $this->response([
                                    'status' => 0,
                                    'message' => 'No Record Found'
                                ], REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $ride_details = $this->Usermodel->rental_ride($booking_id);
                    if (!empty($ride_details))
                    {
                        $booking_status = $ride_details->booking_status;
                        if ($booking_status == 10)
                        {
                            $query = array('driver_id'=>$driver_id,'booking_status'=>32,'last_update_time'=>$last_time_stamp);
                            $this->Drivermodel->ride_rental_later_Accept($query,$booking_id);
                            $user_id = $ride_details->user_id;
                            $message = "Driver Accept Your Ride";
                            $ride_id= (String) $booking_id;
                            $ride_status= "32";
                            $query = $this->Rentalmodel->user_device($user_id);
                            $pem_file = $ride_details->pem_file;
                            if (!empty($query))
                            {
                                foreach ($query as $item)
                                {
                                    $device_id = $item->device_id;
                                    $flag = $item->flag;
                                    if ($flag == 1)
                                    {

                                        $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                    }
                                    else
                                    {
                                        $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                    }
                                }
                            }else{
                                $device_id = $ride_details->device_id;
                                $flag = $ride_details->flag;
                                if ($flag == 1)
                                {
                                    $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                }
                                else
                                {
                                    $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                }
                            }
                            $this->response([
                                'status' => 1,
                                'message' => 'Ride Accpet Succesfully',
                                'details'=>$ride_details
                            ], REST_Controller::HTTP_CREATED);
                        }else{
                            $this->response([
                                'status' => 0,
                                'message' => 'Sorry Ride Expire'
                            ], REST_Controller::HTTP_CREATED);
                        }
                    }else{
                        $this->response([
                                    'status' => 0,
                                    'message' => 'No Record Found'
                                ], REST_Controller::HTTP_CREATED);
                    }
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    public function New_Ride_later_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $city_id = $driver->city_id;
                $new_ride_later = $this->Drivermodel->new_ride_later($driver_id,$city_id);
                if (!empty($new_ride_later))
                {
                    foreach($new_ride_later as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $ride_details = $this->Usermodel->normal_ride($booking_id);
                        }else {
                            $ride_details = $this->Usermodel->rental_ride($booking_id);
                        }
                        $new_ride_later[$key] = $value;
                        $new_ride_later[$key]["detail"] = $ride_details;
                    }
                    $this->response([
                        'status' =>1,
                        'message' => 'New Rides',
                        'details'=>$new_ride_later
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                                'status' => 0,
                                'message' => 'No New Rides'
                            ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    public function Driver_Report_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
               $all_rides = $this->Drivermodel->all_rides($driver_id);
               if($all_rides != 0)
               {
                    $acceptance_rides = $this->Drivermodel->all_acceptance_rides($driver_id);
                    $daily_acceptance = ($acceptance_rides/$all_rides)*100;
                    $daily_acceptance = round($daily_acceptance);
                    $daily_acceptance = $daily_acceptance." %";
               }else{
                    $daily_acceptance = "";
               }
               $avrage_rating = $this->Drivermodel->avarage_rating($driver_id);
               $avrage_rating = round($avrage_rating);
               $online = $this->Drivermodel->online_time($driver_id);
               if(!empty($online)){
                  $total_time  = $online->total_time;
                  $online_time = $online->online_time;
                  if(empty($total_time)){
                      $new_date = date("Y-m-d H:i:s");
                      $datetime1 = new DateTime($online_time);
                      $datetime2 = new DateTime($new_date);
                      $interval = $datetime1->diff($datetime2);
                      $total_time = $interval->format('%h')." Hours ".$interval->format('%i')." Minutes";
                  }
               }else{
                   $total_time = "";
               }
               $data = array('daily_acceptance_rate'=>$daily_acceptance,'avrage_rating'=>$avrage_rating,'online_time'=>$total_time);
                $this->response([
                    'status' => 1,
                    'message' => 'Driver Daily Report',
                    'details' =>$data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    function Driver_Ride_History_post()
    {
        $driver_id = $this->post('driver_id');
        if (!empty($driver_id))
        {
            $data =  $this->Drivermodel->driver_rides($driver_id);
            if(!empty($data))
            {
                foreach ($data as $active_rides)
                {
                    $ride_mode = $active_rides['ride_mode'];
                    $booking_id = $active_rides['booking_id'];
                    if($ride_mode == 1)
                    {
                        $active_normal_ride = $this->Usermodel->completed_normal_ride($booking_id);
                        if(!empty($active_normal_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }else{
                        $active_rental_ride = $this->Usermodel->completed_rental_ride($booking_id);
                        if(!empty($active_rental_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }
                }
                if (!empty($c))
                {
                    foreach($c as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $normal_ride = $this->Usermodel->normal_ride($booking_id);
                            $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                            if (!empty($normal_done_ride))
                            { $begin_lat = $normal_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $normal_ride->pickup_lat;
                                }
                                $begin_long = $normal_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $normal_ride->pickup_long;
                                }
                                $begin_location = $normal_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $normal_ride->pickup_location;
                                }
                                $end_lat = $normal_done_ride->end_lat;
                                if(empty($end_lat))
                                {
                                    $end_lat = $normal_ride->drop_lat;
                                }
                                $end_long = $normal_done_ride->end_long;
                                if(empty($end_long))
                                {
                                    $end_long = $normal_ride->drop_long;
                                }
                                $end_location = $normal_done_ride->end_location;
                                if(empty($end_location))
                                {
                                    $end_location = $normal_ride->drop_location;
                                }
                                $amount = $normal_done_ride->amount;
                                $waiting_price = $normal_done_ride->waiting_price;
                                $ride_time_price =  $normal_done_ride->ride_time_price;
                                $total_amount = $normal_done_ride->total_amount;

                            }else{
                                $begin_lat = $normal_ride->pickup_lat;
                                $begin_long = $normal_ride->pickup_long;
                                $begin_location = $normal_ride->pickup_location;
                                $end_lat = $normal_ride->drop_lat;
                                $end_long = $normal_ride->drop_long;
                                $end_location = $normal_ride->drop_location;
                                $total_amount = "0";
                            }
                            $normal_ride->pickup_lat = $begin_lat;
                            $normal_ride->pickup_long = $begin_long;
                            $normal_ride->pickup_location = $begin_location;
                            $normal_ride->drop_lat = $end_lat;
                            $normal_ride->drop_long = $end_long;
                            $normal_ride->drop_location = $end_location;
                            $normal_ride->total_amount = (string)$total_amount;

                            $rental_ride = array(
                                "rental_booking_id"=> "",
                                "user_id"=> "",
                                "rentcard_id"=> "",
                                "car_type_id"=>"",
                                "booking_type"=>"",
                                "driver_id"=>"",
                                "pickup_lat"=> "",
                                "pickup_long"=> "",
                                "pickup_location"=> "",
                                "start_meter_reading"=>"",
                                "start_meter_reading_image"=> "",
                                "end_meter_reading"=> "",
                                "end_meter_reading_image"=> "",
                                "booking_date"=> "",
                                "booking_time"=> "",
                                "user_booking_date_time"=>"",
                                "last_update_time"=>"",
                                "booking_status"=>"",
                                "booking_admin_status"=>"",
                                "car_type_name"=>"",
                                "car_name_arabic"=> "",
                                "car_type_name_french"=> "",
                                "car_type_image"=> "",
                                "ride_mode"=>"",
                                "car_admin_status"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                                "end_lat"=>"",
                                "end_long"=>"",
                                "end_location"=>"",
                                "final_bill_amount"=> ""
                            );
                        }else{
                            $rental_ride = $this->Usermodel->rental_ride($booking_id);
                            $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                            if(!empty($rental_done_ride))
                            {
                                $begin_lat = $rental_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $rental_ride->pickup_lat;
                                }
                                $begin_long = $rental_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $rental_ride->pickup_long;
                                }
                                $begin_location = $rental_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $rental_ride->pickup_location;
                                }
                                $end_lat = $rental_done_ride->end_lat;
                                $end_long = $rental_done_ride->end_long;
                                $end_location = $rental_done_ride->end_location;
                                $final_bill_amount = $rental_done_ride->final_bill_amount;
                            }else{
                                $begin_lat = $rental_ride->pickup_lat;
                                $begin_long = $rental_ride->pickup_long;
                                $begin_location = $rental_ride->pickup_location;
                                $end_lat = "";
                                $end_long = "";
                                $end_location = "";
                                $final_bill_amount = 0;
                            }
                            $rental_ride->pickup_lat = $begin_lat;
                            $rental_ride->pickup_long = $begin_long;
                            $rental_ride->pickup_location = $begin_location;
                            $rental_ride->end_lat = $end_lat;
                            $rental_ride->end_long = $end_long;
                            $rental_ride->end_location = $end_location;
                            $rental_ride->final_bill_amount = (string)$final_bill_amount;
                            $normal_ride = array(
                                "ride_id"=>"",
                                "user_id"=> "",
                                "coupon_code"=>"",
                                "pickup_lat"=>"",
                                "pickup_long"=>"",
                                "pickup_location"=>"",
                                "drop_lat"=>"",
                                "drop_long"=>"",
                                "drop_location"=>"",
                                "ride_date"=> "",
                                "ride_time"=>"",
                                "last_time_stamp"=>"",
                                "ride_image"=>"" ,
                                "later_date"=> "",
                                "later_time"=>"",
                                "driver_id"=> "",
                                "car_type_id"=>"",
                                "ride_type"=>"",
                                "ride_status"=> "",
                                "reason_id"=> "",
                                "payment_option_id"=>"",
                                "card_id"=> "",
                                "ride_admin_status"=>"",
                                "car_type_name"=> "",
                                "car_name_arabic"=>"",
                                "car_type_name_french"=>"",
                                "car_type_image"=>"",
                                "ride_mode"=> "",
                                "car_admin_status"=>"",
                                "total_amount"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                            );
                        }
                        $c[$key] = $value;
                        $c[$key]["Normal_Ride"] = $normal_ride;
                        $c[$key]["Rental_Ride"] = $rental_ride;
                    }
                    $this->response([
                        'status' =>1,
                        'message' => 'Ride History',
                        'details'=> $data
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                                    'status' =>0,
                                    'message' => 'No Ride History',
                                ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'Sorry You Have Not Rides'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Driver_Active_Ride_History_for_App_post()
    {
        $driver_id = $this->post('driver_id');
        if (!empty($driver_id))
        {
            $data =  $this->Drivermodel->driver_rides($driver_id);
            if(!empty($data))
            {
                foreach ($data as $active_rides)
                {
                    $ride_mode = $active_rides['ride_mode'];
                    $booking_id = $active_rides['booking_id'];
                    if($ride_mode == 1)
                    {
                        $active_normal_ride = $this->Usermodel->driver_active_normal_ride2($booking_id);
                        if(!empty($active_normal_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }else{
                        $active_rental_ride = $this->Usermodel->driver_active_rental_ride($booking_id);
                        if(!empty($active_rental_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }
                }
                if(!empty($c))
                {
                    foreach($c as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $normal_ride = $this->Usermodel->normal_ride($booking_id);
                            $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                            if (!empty($normal_done_ride))
                            { $begin_lat = $normal_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $normal_ride->pickup_lat;
                                }
                                $begin_long = $normal_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $normal_ride->pickup_long;
                                }
                                $begin_location = $normal_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $normal_ride->pickup_location;
                                }
                                $end_lat = $normal_done_ride->end_lat;
                                if(empty($end_lat))
                                {
                                    $end_lat = $normal_ride->drop_lat;
                                }
                                $end_long = $normal_done_ride->end_long;
                                if(empty($end_long))
                                {
                                    $end_long = $normal_ride->drop_long;
                                }
                                $end_location = $normal_done_ride->end_location;
                                if(empty($end_location))
                                {
                                    $end_location = $normal_ride->drop_location;
                                }
                                $amount = $normal_done_ride->amount;
                                $waiting_price = $normal_done_ride->waiting_price;
                                $ride_time_price =  $normal_done_ride->ride_time_price;
                                $total_amount = $normal_done_ride->total_amount;

                            }else{
                                $begin_lat = $normal_ride->pickup_lat;
                                $begin_long = $normal_ride->pickup_long;
                                $begin_location = $normal_ride->pickup_location;
                                $end_lat = $normal_ride->drop_lat;
                                $end_long = $normal_ride->drop_long;
                                $end_location = $normal_ride->drop_location;
                                $total_amount = "0";
                            }
                            $normal_ride->pickup_lat = $begin_lat;
                            $normal_ride->pickup_long = $begin_long;
                            $normal_ride->pickup_location = $begin_location;
                            $normal_ride->drop_lat = $end_lat;
                            $normal_ride->drop_long = $end_long;
                            $normal_ride->drop_location = $end_location;
                            $normal_ride->total_amount = (string)$total_amount;

                            $rental_ride = array(
                                "rental_booking_id"=> "",
                                "user_id"=> "",
                                "rentcard_id"=> "",
                                "car_type_id"=>"",
                                "booking_type"=>"",
                                "driver_id"=>"",
                                "pickup_lat"=> "",
                                "pickup_long"=> "",
                                "pickup_location"=> "",
                                "start_meter_reading"=>"",
                                "start_meter_reading_image"=> "",
                                "end_meter_reading"=> "",
                                "end_meter_reading_image"=> "",
                                "booking_date"=> "",
                                "booking_time"=> "",
                                "user_booking_date_time"=>"",
                                "last_update_time"=>"",
                                "booking_status"=>"",
                                "booking_admin_status"=>"",
                                "car_type_name"=>"",
                                "car_name_arabic"=> "",
                                "car_type_name_french"=> "",
                                "car_type_image"=> "",
                                "ride_mode"=>"",
                                "car_admin_status"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                                "end_lat"=>"",
                                "end_long"=>"",
                                "end_location"=>"",
                                "final_bill_amount"=> ""
                            );
                        }else{
                            $rental_ride = $this->Usermodel->rental_ride($booking_id);
                            $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                            if(!empty($rental_done_ride))
                            {
                                $begin_lat = $rental_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $rental_ride->pickup_lat;
                                }
                                $begin_long = $rental_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $rental_ride->pickup_long;
                                }
                                $begin_location = $rental_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $rental_ride->pickup_location;
                                }
                                $end_lat = $rental_done_ride->end_lat;
                                $end_long = $rental_done_ride->end_long;
                                $end_location = $rental_done_ride->end_location;
                                $final_bill_amount = $rental_done_ride->final_bill_amount;
                            }else{
                                $begin_lat = $rental_ride->pickup_lat;
                                $begin_long = $rental_ride->pickup_long;
                                $begin_location = $rental_ride->pickup_location;
                                $end_lat = "";
                                $end_long = "";
                                $end_location = "";
                                $final_bill_amount = 0;
                            }
                            $rental_ride->pickup_lat = $begin_lat;
                            $rental_ride->pickup_long = $begin_long;
                            $rental_ride->pickup_location = $begin_location;
                            $rental_ride->end_lat = $end_lat;
                            $rental_ride->end_long = $end_long;
                            $rental_ride->end_location = $end_location;
                            $rental_ride->final_bill_amount = (string)$final_bill_amount;
                            $normal_ride = array(
                                "ride_id"=>"",
                                "user_id"=> "",
                                "coupon_code"=>"",
                                "pickup_lat"=>"",
                                "pickup_long"=>"",
                                "pickup_location"=>"",
                                "drop_lat"=>"",
                                "drop_long"=>"",
                                "drop_location"=>"",
                                "ride_date"=> "",
                                "ride_time"=>"",
                                "last_time_stamp"=>"",
                                "ride_image"=>"" ,
                                "later_date"=> "",
                                "later_time"=>"",
                                "driver_id"=> "",
                                "car_type_id"=>"",
                                "ride_type"=>"",
                                "ride_status"=> "",
                                "reason_id"=> "",
                                "payment_option_id"=>"",
                                "card_id"=> "",
                                "ride_admin_status"=>"",
                                "car_type_name"=> "",
                                "car_name_arabic"=>"",
                                "car_type_name_french"=>"",
                                "car_type_image"=>"",
                                "ride_mode"=> "",
                                "car_admin_status"=>"",
                                "total_amount"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                            );
                        }
                        $c[$key] = $value;
                        $c[$key]["Normal_Ride"] = $normal_ride;
                        $c[$key]["Rental_Ride"] = $rental_ride;
                    }
                    $this->response([
                        'result' =>"1",
                        'message' => 'Active Ride History',
                        'details'=> $c
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'result' =>"0",
                        'message' => 'Sorry You Have Active Rides'
                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->response(['result'=>"0",'message'=>'No Rides'],REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'result' =>"0",
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }


    function Driver_Active_Ride_History_post()
    {
        $driver_id = $this->post('driver_id');
        if (!empty($driver_id))
        {
            $data =  $this->Drivermodel->driver_rides($driver_id);
            if(!empty($data))
            {
                foreach ($data as $active_rides)
                {
                    $ride_mode = $active_rides['ride_mode'];
                    $booking_id = $active_rides['booking_id'];
                    if($ride_mode == 1)
                    {
                        $active_normal_ride = $this->Usermodel->driver_active_normal_ride($booking_id);
                        if(!empty($active_normal_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }else{
                        $active_rental_ride = $this->Usermodel->driver_active_rental_ride($booking_id);
                        if(!empty($active_rental_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }
                }
                if(!empty($c))
                {
                    foreach($c as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $normal_ride = $this->Usermodel->normal_ride($booking_id);
                            $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                            if (!empty($normal_done_ride))
                            { $begin_lat = $normal_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $normal_ride->pickup_lat;
                                }
                                $begin_long = $normal_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $normal_ride->pickup_long;
                                }
                                $begin_location = $normal_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $normal_ride->pickup_location;
                                }
                                $end_lat = $normal_done_ride->end_lat;
                                if(empty($end_lat))
                                {
                                    $end_lat = $normal_ride->drop_lat;
                                }
                                $end_long = $normal_done_ride->end_long;
                                if(empty($end_long))
                                {
                                    $end_long = $normal_ride->drop_long;
                                }
                                $end_location = $normal_done_ride->end_location;
                                if(empty($end_location))
                                {
                                    $end_location = $normal_ride->drop_location;
                                }
                                $amount = $normal_done_ride->amount;
                                $waiting_price = $normal_done_ride->waiting_price;
                                $ride_time_price =  $normal_done_ride->ride_time_price;
                                $total_amount = $normal_done_ride->total_amount;

                            }else{
                                $begin_lat = $normal_ride->pickup_lat;
                                $begin_long = $normal_ride->pickup_long;
                                $begin_location = $normal_ride->pickup_location;
                                $end_lat = $normal_ride->drop_lat;
                                $end_long = $normal_ride->drop_long;
                                $end_location = $normal_ride->drop_location;
                                $total_amount = "0";
                            }
                            $normal_ride->pickup_lat = $begin_lat;
                            $normal_ride->pickup_long = $begin_long;
                            $normal_ride->pickup_location = $begin_location;
                            $normal_ride->drop_lat = $end_lat;
                            $normal_ride->drop_long = $end_long;
                            $normal_ride->drop_location = $end_location;
                            $normal_ride->total_amount = (string)$total_amount;

                            $rental_ride = array(
                                "rental_booking_id"=> "",
                                "user_id"=> "",
                                "rentcard_id"=> "",
                                "car_type_id"=>"",
                                "booking_type"=>"",
                                "driver_id"=>"",
                                "pickup_lat"=> "",
                                "pickup_long"=> "",
                                "pickup_location"=> "",
                                "start_meter_reading"=>"",
                                "start_meter_reading_image"=> "",
                                "end_meter_reading"=> "",
                                "end_meter_reading_image"=> "",
                                "booking_date"=> "",
                                "booking_time"=> "",
                                "user_booking_date_time"=>"",
                                "last_update_time"=>"",
                                "booking_status"=>"",
                                "booking_admin_status"=>"",
                                "car_type_name"=>"",
                                "car_name_arabic"=> "",
                                "car_type_name_french"=> "",
                                "car_type_image"=> "",
                                "ride_mode"=>"",
                                "car_admin_status"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                                "end_lat"=>"",
                                "end_long"=>"",
                                "end_location"=>"",
                                "final_bill_amount"=> ""
                            );
                        }else{
                            $rental_ride = $this->Usermodel->rental_ride($booking_id);
                            $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                            if(!empty($rental_done_ride))
                            {
                                $begin_lat = $rental_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $rental_ride->pickup_lat;
                                }
                                $begin_long = $rental_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $rental_ride->pickup_long;
                                }
                                $begin_location = $rental_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $rental_ride->pickup_location;
                                }
                                $end_lat = $rental_done_ride->end_lat;
                                $end_long = $rental_done_ride->end_long;
                                $end_location = $rental_done_ride->end_location;
                                $final_bill_amount = $rental_done_ride->final_bill_amount;
                            }else{
                                $begin_lat = $rental_ride->pickup_lat;
                                $begin_long = $rental_ride->pickup_long;
                                $begin_location = $rental_ride->pickup_location;
                                $end_lat = "";
                                $end_long = "";
                                $end_location = "";
                                $final_bill_amount = 0;
                            }
                            $rental_ride->pickup_lat = $begin_lat;
                            $rental_ride->pickup_long = $begin_long;
                            $rental_ride->pickup_location = $begin_location;
                            $rental_ride->end_lat = $end_lat;
                            $rental_ride->end_long = $end_long;
                            $rental_ride->end_location = $end_location;
                            $rental_ride->final_bill_amount = (string)$final_bill_amount;
                            $normal_ride = array(
                                "ride_id"=>"",
                                "user_id"=> "",
                                "coupon_code"=>"",
                                "pickup_lat"=>"",
                                "pickup_long"=>"",
                                "pickup_location"=>"",
                                "drop_lat"=>"",
                                "drop_long"=>"",
                                "drop_location"=>"",
                                "ride_date"=> "",
                                "ride_time"=>"",
                                "last_time_stamp"=>"",
                                "ride_image"=>"" ,
                                "later_date"=> "",
                                "later_time"=>"",
                                "driver_id"=> "",
                                "car_type_id"=>"",
                                "ride_type"=>"",
                                "ride_status"=> "",
                                "reason_id"=> "",
                                "payment_option_id"=>"",
                                "card_id"=> "",
                                "ride_admin_status"=>"",
                                "car_type_name"=> "",
                                "car_name_arabic"=>"",
                                "car_type_name_french"=>"",
                                "car_type_image"=>"",
                                "ride_mode"=> "",
                                "car_admin_status"=>"",
                                "total_amount"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                            );
                        }
                        $c[$key] = $value;
                        $c[$key]["Normal_Ride"] = $normal_ride;
                        $c[$key]["Rental_Ride"] = $rental_ride;
                    }
                    $this->response([
                        'result' =>"1",
                        'message' => 'Active Ride History',
                        'details'=> $c
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'result' =>"0",
                        'message' => 'Sorry You Have Active Rides'
                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->response(['result'=>"0",'message'=>'No Rides'],REST_Controller::HTTP_CREATED);
            }
            }else{
                    $this->response([
                        'result' =>"0",
                        'message' => 'Required Field Missing'
                    ], REST_Controller::HTTP_CREATED);
        }
    }

    function Change_Password_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $new_password = $this->post('new_password');
        $old_password = $this->post('old_password');
        if(!empty($driver_id) && !empty($driver_token) && !empty($old_password) && !empty($new_password))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data = $this->Drivermodel->change_password($driver_id,$new_password,$old_password);
                if (!empty($data))
                {
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Your Password Change Successfully'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Password Not Change'
                    ], REST_Controller::HTTP_CREATED);
                }


            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $app_id="2";
        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

        $headers = array (
            'Authorization: key=AAAAScCQu44:APA91bGv5aF9Nc7VwoD1BiwEXOlzUmduxil63c6TrYymDtBWlT91AL7oSoe9yW-ihdibptX4X-g3pVq10gNC3swN_pWI1QGTmTqIc7DoYjr6gqJTDF0aNaJhMpTQJaeOhQbQQnPUoDxb',
            'Content-Type: application/json' );
        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationDriver($did,$msg,$ride_id,$ride_status,$pem)
    {
        $passphrase = 'programmer';
        $message = $msg;

        $ctx = stream_context_create();
        if($pem == 1){
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_driver_distri.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }else{
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_driver_debug.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status'=>$ride_status);

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);
    }

    function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $app_id="1";

        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

        $headers = array (
            'Authorization: key=AAAAScCQu44:APA91bGv5aF9Nc7VwoD1BiwEXOlzUmduxil63c6TrYymDtBWlT91AL7oSoe9yW-ihdibptX4X-g3pVq10gNC3swN_pWI1QGTmTqIc7DoYjr6gqJTDF0aNaJhMpTQJaeOhQbQQnPUoDxb',
            'Content-Type: application/json' );

        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationCustomer($did,$msg,$ride_id,$ride_status,$pem)
    {
        $passphrase = 'programmer';
        $message = $msg;

        $ctx = stream_context_create();
        if($pem == 1)
        {
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_user_distri.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }else{
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_user_debug.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);
    }

    public function generateRandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
?>