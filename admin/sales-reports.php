<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="SELECT * FROM driver_earnings WHERE MONTH(date)= 8";
$result = $db->query($query);
$list = $result->rows;
$date_amount = array();
foreach ($list as $key=>$value)
{
    $date_amount[$value['date']]['date']= $value['date'];
    $date_amount[$value['date']]['total_amount'][]= $value['total_amount'];
    $date_amount[$value['date']]['amount'][]= $value['amount'];
}
$dates = array();
foreach($date_amount as $v){
    $total_amount = array_sum($v['total_amount']);
    $amount = array_sum($v['amount']);
    $company_cut = $total_amount-$amount;
    $dates[] = array('date'=>$v['date'],'total_amount'=>$total_amount,'company_cut'=>$company_cut);
}

$query="select * from city";
$result = $db->query($query);
$list1=$result->rows;
foreach ($list1 as $keys=>$login)
{
    $city_id = $login['city_id'];
    $query = "select * from ride_table INNER JOIN driver ON ride_table.driver_id=driver.driver_id WHERE driver.city_id='$city_id'";
    $result = $db->query($query);
    $ride = $result->num_rows;
    $list1[$keys]=$login;
    $list1[$keys]["total_rides"]=$ride;
}
$query="select * from car_type";
$result = $db->query($query);
$list2=$result->rows;
foreach ($list2 as $keyss=>$login)
{
    $car_type_id = $login['car_type_id'];
    $query = "select * from ride_table WHERE car_type_id='$car_type_id'";
    $result = $db->query($query);
    $ride = $result->num_rows;
    $list2[$keyss]=$login;
    $list2[$keyss]["total_rides"]=$ride;
}
$query="select * from payment_option";
$result = $db->query($query);
$list3=$result->rows;
foreach ($list3 as $k=>$login)
{
    $payment_option_id = $login['payment_option_id'];
    $query = "select * from ride_table WHERE payment_option_id='$payment_option_id'";
    $result = $db->query($query);
    $ride = $result->num_rows;
    $list3[$k]=$login;
    $list3[$k]["total_rides"]=$ride;
}
$query = "select * from user";
$result = $db->query($query);
$user = $result->num_rows;
$query = "select * from driver";
$result = $db->query($query);
$driver = $result->num_rows;
?>
<div class="wraper container-fluid" >

    <div class="row col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Total Revenue Amount</h3>
            </div>
            <div class="panel-body">
                <div id="payment"  style="width: 500px; height: 400px;"></div>
            </div>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">City Wise Revenue</h3>
                </div>
                <div class="panel-body">
                    <div id="city_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Car Wise Revenue</h3>
                </div>
                <div class="panel-body">
                    <div id="cartype_div"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Payment Method Wise Revenue</h3>
                </div>
                <div class="panel-body">
                    <div id="payment_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Application Usage</h3>
                </div>
                <div class="panel-body">
                    <div id="application_div"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Application', 'Application User Percantage'],
            ['User', <?php echo $user?>],
            ['Driver', <?php echo $driver?>]
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('application_div'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Payment Type', 'Payment Type'],
            <?php foreach ($list3 as $data){?>
            ['<?php echo $data['payment_option_name']?>', <?php echo $data['total_rides']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('payment_div'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Car Type', 'Ride Per Car Type'],
            <?php foreach ($list2 as $data){?>
            ['<?php echo $data['car_type_name']?>', <?php echo $data['total_rides']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('cartype_div'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['City Name', 'Ride Per City'],
            <?php foreach ($list1 as $data){?>
            ['<?php echo $data['city_name']?>', <?php echo $data['total_rides']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('city_div'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Date", "Date Wise  Revenue","Date Wise Copmpay Revenue", { role: "style" } ],
            <?php foreach ($dates as $value){ ?>
            ["<?php echo $value['date']?>",<?php echo $value['total_amount']?>, <?php echo $value['company_cut']?>,"#3F33FF"],
            <?php } ?>
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            width: 1400,
            height: 400,
            bar: {groupWidth: "50%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("payment"));
        chart.draw(view, options);
    }
</script>