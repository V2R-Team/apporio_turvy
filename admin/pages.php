<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from pages";
$result = $db->query($query);
$list = $result->rows;
?>


<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">CMS Pages</h3>
                 <span class="tp_rht">
            <a href="home.php?pages=edit-pages" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Page"><i class="fa fa-plus"></i></a>
          </span>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Page Name</th>
                                        <th>Page Title</th>
                                        <th width="4%">Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $page){?>
                                        <tr>
                                            <td><?php echo $page['name'];?></td>
                                            <td>
                                                <?php
                                                $title = $page['title'];
                                                echo $title;
                                                ?>
                                            </td>
                                            <td>
<div class="row action_row">
<a href="home.php?pages=edit-pages&page_id=<?=$page['page_id']?>" data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a>
</div>
                                           
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</section>
<!-- Main Content Ends -->

</body></html>