<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$sos_request_id = $_GET['id'];

$sql1 = "SELECT * FROM sos_request WHERE sos_request_id='$sos_request_id'";
$Query = $db->query($sql1);
$list2 = $Query->row;
$latitude = $list2['latitude'];
$longitude = $list2['longitude'];
?>
<style>
    #map {
        height: 650px;
        width: 100%;
    }
</style>
<script>
    function initMap() {
        var uluru = {lat: <?php echo $latitude ?>, lng: <?php echo $longitude ?> };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg&callback=initMap">
</script>
<div class="wraper container-fluid">
    <div class="row">
        <div id="map"></div>
    </div>
</div>

