<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from user WHERE user_signup_type=1";
$result = $db->query($query);
$normal =$result->num_rows;
$query="select * from user WHERE user_signup_type=2";
$result = $db->query($query);
$facebook =$result->num_rows;
$query="select * from user WHERE user_signup_type=3";
$result = $db->query($query);
$google =$result->num_rows;
$query="select * from user WHERE login_logout=1";
$result = $db->query($query);
$login_logout =$result->num_rows;
$query="select * from user WHERE login_logout !=1";
$result = $db->query($query);
$logout =$result->num_rows;
$query="select * from user WHERE status =1";
$result = $db->query($query);
$active =$result->num_rows;
$query="select * from user WHERE status !=1";
$result = $db->query($query);
$deactive =$result->num_rows;
?>
<div class="wraper container-fluid" >

    <div class="row col-md-12">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Signup Comparison Month Wise</h3>
                </div>
                <div class="panel-body">
                    <div id="columnchart_values"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Signup </h3>
                </div>
                <div class="panel-body">
                    <div id="piechart" style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Online/Offline</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Active/Deactive</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div1"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Signup Type', 'User Per Signup Type'],
            ['Normal', <?php echo $normal?>],
            ['Facebook', <?php echo $facebook?>],
            ['Google', <?php echo $google?>]
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Month", "User Per Month", { role: "style" } ],
            ["January", 105, "#3F33FF"],
            ["February", 100, "#3F33FF"],
            ["March", 130, "#3F33FF"],
            ["May", 200, "color: #3F33FF"],
            ["June", 250, "color: #3F33FF"],
            ["July", 125, "color: #3F33FF"],
            ["August", 70, "color: #3F33FF"],
            ["September", 175, "color: #3F33FF"],
            ["October", 180, "color: #3F33FF"],
            ["November", 190, "color: #3F33FF"],
            ["December", 160, "color: #3F33FF"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            width: 600,
            height: 400,
            bar: {groupWidth: "50%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
</script>
<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Online Offline', 'User Online Offline'],
            ['Login', <?php echo $login_logout?>],
            ['Logout', <?php echo $logout?>]
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data,options);
    }
</script>
<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Active Dective', 'Active Dective'],
            ['Active', <?php echo $active?>],
            ['Dective', <?php echo $deactive?>]
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('chart_div1'));
        chart.draw(data,options);
    }
</script>
</section>
</body></html>
